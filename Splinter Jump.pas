﻿{$mainresource icongame.res}
{$platformtarget x86}
{$reference 'Bass.net.dll'}

uses
  
  System.Threading,
  un4seen.Bass,
  GraphABC, 
  ABCSprites, 
  Events, 
  ABCObjects, 
  ABCButtons, 
  System.Drawing, 
  Timers;
  
const
  
  n=200; //платформы
  b=100; //боссы
  ch=100; //сыры 
  d=20; //напитки 
  
function BASS_Init(device: integer; freq: integer; flags: Un4seen.Bass.BASSInit; win: System.IntPtr): boolean;
begin
 BASS_Init:=un4seen.Bass.bass.BASS_Init(device,freq,flags,win);
end;

function BASS_StreamCreateFile(lfile: string; offset: int64; length: int64; flags: Un4seen.Bass.BASSFlag): integer;
begin
 BASS_StreamCreateFile:=un4seen.Bass.Bass.BASS_StreamCreateFile(lfile,offset,length,flags);
end;  
  
type
Seconder = record
ms: integer:=0;
s: integer:=0;
m: integer:=0;
end;
  
Splinter = class (SpriteABC)
private
  Suriken: SpriteABC; //Сюрикен 
  MSTimer: Timer; //Таймер для сюрикена
  MSFace: SpriteABC; //Иконка
  MSFaceFrame: RectangleABC; //Рамка для индикатора жизней
  MSFaceLive: RectangleABC; //Индикатор жизней
  MSFaceDrink: RectangleABC; //Индикатор действия напитка
  procedure ForTimer; 
public
  constructor Create(x,y: integer; fname: string);
  destructor Destroy;
  procedure GameOver;
  procedure Rightg(j: integer);
  procedure Leftg(j: integer);
  procedure Strike;
  procedure Jump(j: integer);
end;

Platform = class (PictureABC)
private
  vector: integer;
  s: integer;
  ora: boolean;
public
  constructor Create(x: integer; y: integer; fnamei: integer);
  procedure Downing(vxy: integer);
  procedure ForBlueTimer;
  procedure Blue(f: boolean);
  procedure ForPurpleTimer;
  procedure Purple(f: boolean);
  procedure Red(f: boolean);
  procedure ForRedTimer;
  procedure Orange(f: boolean);
  procedure ForOrangeTimer;
  destructor Destroy;
end;

Button = class (RoundRectABC)
private
  b: RoundRectABC;
  color1, color2: graphabc.Color;
  OnClick: procedure;
public
  constructor Create(x: integer; y: integer; w: integer; h: integer; txt: string; cl1: graphabc.color; cl2: graphabc.color);
  procedure Light;
  procedure OffLight;
  destructor Destroy;
 end;

BackGrounds = class (PictureABC)
private
   h:=-1;
public
  ///Координаты Бэка - x и y, fname - путь к картинке, fmname - путь к фоновой музыке
  constructor Create(x: integer; y: integer; fname: string; fmname: string);
  constructor Create(x: integer; y: integer; fname: string);
  ///Останавливает воспроизведение фоновой музыки
  procedure Stop;
  destructor Destroy;
end;

UnFriend = class (SpriteABC)
private
  lvlz, vek: integer;
  TimerV: Timer;
  procedure ForTimer;
public
  procedure MoveGame;
  constructor Create(x,y,z: integer);
  destructor Destroy;
end;

var 

    sysinptr:=system.IntPtr.Zero;
    Boss: array [1..b] of UnFriend;
    x, y: integer;
    
constructor Splinter.Create(x,y: integer; fname: string);
begin
inherited;
Self.Transparent:=True;

Suriken := SpriteABC.Create(-100,-100,'Splinter/Suriken.spinf');
Suriken.Transparent:=True;

MSFace := SpriteABC.Create(30,3,'Splinter/MSFace.spinf');

MSFaceFrame := RectangleABC.Create(60,3,44,30,clwhite);

MSFaceLive := RectangleABC.Create(61,4,43,15,clred);
MSFaceLive.Bordered:=False;

MSFaceDrink := RectangleABC.Create(61,18,43,15,cllightgreen);
MSFaceDrink.Bordered:=False;
MSFaceDrink.Width:=1;
end;

destructor Splinter.Destroy;
begin
inherited;
end;

procedure Splinter.ForTimer;
begin
if ((Suriken.Top<0) or (Suriken.Left<0)) and (x<0) then begin MSTimer.Enabled:=False; Suriken.MoveTo(-36,-36); end;
if ((Suriken.Top<0) or (WindowWidth-Suriken.Left<0)) and (x>0) then begin MSTimer.Enabled:=False; Suriken.MoveTo(-36,-36); end;
Suriken.MoveOn(2*x,2*y);
end; 

procedure Splinter.GameOver;
var GameOver: TextABC;
    GameOverPicture: PictureABC;
begin
MoveTo(1000,-100);
GameOverPicture := PictureABC.Create(0,0,'Pictures/GameOver.jpg');
GameOverPicture.Width:=WindowWidth;
GameOverPicture.Height:=WindowHeight;
GameOver := TextABC.Create(round(windowwidth/7), round(windowheight/2.5),56,'Game Over',clgreen);
Destroy;
end;

procedure Splinter.Rightg(j: integer);
var i: integer;
begin
StateName:='Right'; 
For i:=1 to 8 do
  begin
  MoveOn(1*j,0); 
  end;
if Left>WindowWidth then MoveOn(-WindowWidth-36,0);
end;

procedure Splinter.Leftg(j: integer);
var i: integer;
begin
StateName:='Left'; 
For i:=1 to 8 do
  begin
  MoveOn(-1*j,0); 
  end;
if Left<-36 then MoveOn(WindowWidth+36,0);
end;

procedure Splinter.Strike;
begin
Suriken.MoveTo(Left+18-9,Top-9);
y:=-5; x:=random(11)-5; 
if MSTimer=nil then MSTimer := Timer.Create(50,ForTimer);
MSTimer.Enabled:=True;
end;

procedure Splinter.Jump(j: integer);
var i: integer;
begin
case j of 
1: begin For i:=0 to 16 do
    begin
    MoveOn(0,-(17-i));
    sleep(35);
    end; end;
2: begin For i:=0 to 20 do
    begin
    MoveOn(0,-(21-i));
    sleep(35);
    end; end;
3: begin For i:=0 to 25 do
    begin
    MoveOn(0,-(26-i));
    sleep(35);
    end; end;
end;
end;

constructor Platform.Create(x: integer; y: integer; fnamei: integer);
var fname: string;
begin
case fnamei of 
  1: fname:='Platform/Green.png';
  2: begin fname:='Platform/Blue.png'; Blue(True); end;
  3: begin fname:='Platform/Purple.png'; Purple(True); end;
  4: begin fname:='Platform/Red.png'; Red(True); end;
  5: begin fname:='Platform/Orange.png'; Orange(True); end;
  end;
Init(x,y,fname); 
InternalDraw;  
end;

procedure Platform.Downing(vxy: integer);
var i: integer;
begin
For i:=1 to 1 do
  begin
  MoveOn(0,round(vxy/1));
  end;
end;

procedure Platform.ForBlueTimer;
var vector: integer:=-2;
begin
MoveOn(vector,0);
if Left<=-90 then begin MoveOn(WindowWidth+90,0);  end;
if Left>=WindowWidth+90 then begin MoveOn(-WindowWidth,0); end;
end;

procedure Platform.Blue(f: boolean);
var TimerP: Timer;
begin
if TimerP=nil then TimerP := Timer.Create(20,ForBlueTimer);
if f=True then TimerP.Enabled:=True else TimerP.Enabled:=False;
end;

procedure Platform.ForPurpleTimer;
var vector: integer:=2;
begin
MoveOn(vector,0);
if Left<=-90 then begin MoveOn(WindowWidth,0);  end;
if Left>=WindowWidth then begin MoveOn(-WindowWidth-90,0); end;
end;

procedure Platform.Purple(f: boolean);
var TimerP2: Timer;
begin
if TimerP2=nil then TimerP2 := Timer.Create(20,ForPurpleTimer);
if f=True then TimerP2.Enabled:=True else TimerP2.Enabled:=False;
end;

procedure Platform.ForRedTimer;
begin
inc(s);
MoveOn(0,vector);
if s>=20 then begin s:=0; if vector=2 then vector:=-2 else vector:=2; end;
end;

procedure Platform.Red(f: boolean);
var TimerP3: Timer;
begin
if TimerP3=nil then TimerP3 := Timer.Create(random(50)+50,ForRedTimer);
if f=True then TimerP3.Enabled:=True else TimerP3.Enabled:=False;
end;

procedure Platform.ForOrangeTimer;
begin
if ora=True then
  begin
  Width:=90;
  Height:=20;
  ora:=False;
  MoveOn(-35,35);
  end
else
  begin
  Width:=20;
  Height:=90;
  ora:=True;
  MoveOn(35,-35);
  end;
end;

procedure Platform.Orange(f: boolean);
var TimerP4: Timer;
begin
if TimerP4=nil then TimerP4 := Timer.Create(random(2700)+300,ForOrangeTimer);
if f=True then TimerP4.Enabled:=True else TimerP4.Enabled:=False;
end;


destructor Platform.Destroy;
begin
inherited; 
end;

constructor BackGrounds.Create(x: integer; y: integer; fname: string; fmname: string);
begin
BASS_Init(-1,4410,un4seen.Bass.BASSInit.BASS_DEVICE_SPEAKERS,sysinptr);
h:=BASS_StreamCreateFile(fmname,0,0,un4seen.Bass.BASSFlag.BASS_SAMPLE_LOOP);
un4seen.Bass.bass.BASS_ChannelPlay(h,true);
Init(x,y,fname);
//Width := windowwidth; 
//Height := windowheight;  
InternalDraw;
end;

constructor BackGrounds.Create(x: integer; y: integer; fname: string);
begin
Init(x,y,fname);
//Width := windowwidth; 
//Height := windowheight;  
InternalDraw;
end;

procedure BackGrounds.Stop;
begin
un4seen.Bass.Bass.BASS_ChannelStop(h);
un4seen.Bass.Bass.BASS_StreamFree(h);
end;

destructor BackGrounds.Destroy;
begin
  inherited;
end;

constructor Button.Create(x: integer; y: integer; w: integer; h: integer; txt: string; cl1: graphabc.Color; cl2: graphabc.Color);
begin
b := RoundRectABC.Create(x, y, w, h, 5, cl1);
b.Text:=txt;
color1:=cl1;
color2:=cl2;
end;

procedure Button.Light;
begin
b.Color:=color2;
end;

procedure Button.OffLight;
begin
b.Color:=color1;
end;

destructor Button.Destroy;
begin
  b.Destroy;
end;  

procedure UnFriend.ForTimer;
begin
   Self.MoveOn(vek,0);
   If Self.Left<-Self.Width*lvlz then begin vek:=5; Self.StateName:='Right'; MoveTo(Left,round(WindowWidth/2)-Self.Height); end;
   If Self.Left>WindowWidth+Self.Width*lvlz then begin vek:=-5; Self.StateName:='Left'; MoveTo(Left,round(WindowWidth/2)-Self.Height); end;
end;

procedure UnFriend.MoveGame;
begin
if vek<0 then Self.MoveTo(-Self.Width*lvlz,Top) else Self.MoveTo(WindowWidth+Self.Width*lvlz,Top);
end;

constructor UnFriend.Create(x,y,z: integer);
var fname: string;
begin
case z of 
  1: begin fname:='UnFriend/Mouser/Mouser.spinf'; lvlz:=7; end;
  2: begin fname:='UnFriend/Foot/Foot.spinf'; lvlz:=7; end;
  3: begin fname:='UnFriend/Elite Foot/EliteFoot.spinf'; lvlz:=6; end;
  4: begin fname:='UnFriend/Hun/Hun.spinf'; lvlz:=6; end;
  5: begin fname:='UnFriend/Shredd/Shredd.spinf'; lvlz:=5; end;
  end;
var s := ExtractFileExt(fname);
   InitWithStates(x,y,fname);
   Self.Transparent := True;
TimerV := Timer.Create(50,ForTimer);
TimerV.Enabled:=True;   
vek:=-5;
end;

destructor UnFriend.Destroy;
begin
inherited;
end;

var 

    MS: Splinter; //Сплинтер 
    Recordsm: array [1..10,1..3] of integer; //Массив для записи рекордов
    SInG: array [1..10] of integer; //Массив звуков
    Back: array [0..10] of BackGrounds; //Массив бэков
    Start, Exitb, BattleMode, Records: Button; //Кнопки
    Table: array [1..n] of platform; //Массив платформ
    TimerHelp: System.Threading.Thread; //Вспомогательный поток
    TimerG: Timer; //Таймер движка
    Logo: Icon; //Лого 
    k: boolean;
    m: boolean:=False;
    MenuP, TV: pictureABC;
    Cheese, h, time: TextABC;
    dt: Seconder;
    hi, schh, lvlshets, jumple, sec, but: integer;
    CheeseH: array [1..ch] of PictureABC;
    NormalDrink: array [1..d] of PictureABC;
    UranDrink: array [1..d] of PictureABC;

procedure SetAllVolume(v: real);
begin
For var i:=1 to 10 do
un4seen.Bass.Bass.BASS_ChannelSetAttribute(SInG[i], un4seen.Bass.BASSAttribute.Bass_Attrib_Vol, v);
un4seen.Bass.Bass.BASS_ChannelSetAttribute(Back[lvlshets].h, un4seen.Bass.BASSAttribute.Bass_Attrib_Vol, v);
end;

procedure PlusAllVolume(v: real);
var k: single;
begin
un4seen.Bass.Bass.BASS_ChannelGetAttribute(Back[lvlshets].h, un4seen.Bass.BASSAttribute.Bass_Attrib_Vol, k);
SetAllVolume(k+v);
end;

procedure Lvlschet; forward;

procedure SaveGame;
var i, j: integer;
begin
assign(output,'records.dat');
rewrite(output);
for i:=1 to 10 do
   begin 
   for j:=1 to 3 do
   write(output,recordsm[i,j],' ');
   end;
Close(output);   

var bmp := new Bitmap(GraphABC.MainForm.Width, GraphABC.MainForm.Height);
var gfx := Graphics.FromImage(bmp);
gfx.CopyFromScreen(GraphABC.MainForm.Location.X, GraphABC.MainForm.Location.Y, 0, 0, GraphABC.MainForm.Size, CopyPixelOperation.SourceCopy);
bmp.Save('Screens/'+(lvlshets-1).ToString+System.DateTime.Now.Year.ToString+System.DateTime.Now.Day.ToString+
System.DateTime.Now.Hour.ToString+System.DateTime.Now.Minute.ToString+
System.DateTime.Now.Second.ToString+'.bmp');
end;

procedure CloseGame;
begin
SaveGame;
CloseWindow;   
end;

procedure GameEnd;
var i: integer;
      Pic: array [1..9] of BackGrounds;
      Text: array [1..9] of TextABC;
begin
Back[lvlshets-1].Stop;
Back[lvlshets-1].Visible:=False;
{For i:=1 to n do
   begin
   if Table[n]=nil then else Table[n].Destroy;
   end;
 TV.Visible:=False;
For i:=1 to ch do
      if cheeseH[i]=nil then else cheeseH[i].Visible:=False; 
MS.Visible:=False;     }
un4seen.Bass.bass.BASS_ChannelPlay(SInG[7],true);

Pic[1] := BackGrounds.Create(-540,0,'Pictures/01.jpg');
Pic[2] := BackGrounds.Create(-540,0,'Pictures/02.jpg');
Pic[3] := BackGrounds.Create(-540,0,'Pictures/03.jpg');
Pic[4] := BackGrounds.Create(-540,0,'Pictures/04.jpg');
Pic[5] := BackGrounds.Create(-540,0,'Pictures/05.jpg');
Pic[6] := BackGrounds.Create(-540,0,'Pictures/06.jpg');
Pic[7] := BackGrounds.Create(-540,0,'Pictures/07.jpg');
Pic[8] := BackGrounds.Create(-540,0,'Pictures/08.jpg');
Pic[9] := BackGrounds.Create(-540,0,'Pictures/09.jpg');

Pic[1].MoveTo(0,0); Pic[1].ToFront;
Text[1] := TextABC.Create(70,570,15,'- И что же было дальше, учитель? - ЧН',clwhite);
sleep(4000);

Pic[2].MoveTo(0,0); Pic[2].ToFront;
Text[1].MoveTo(-540,0);
Text[2] := TextABC.Create(70,570,15,'- Хммм.... - МС',clwhite);
sleep(4000);

Pic[3].MoveTo(0,0); Pic[3].ToFront;
Text[2].MoveTo(-540,0);
Text[3] := TextABC.Create(20,570,14,'- Я не помню, помню лишь то, что потом я проснулся, - МС',clwhite);
sleep(4000);

Pic[4].MoveTo(0,0); Pic[4].ToFront;
Text[3].MoveTo(-540,0);
Text[4] := TextABC.Create(70,570,15,'- Понятно всё, он гонит, - ЧН',clwhite);
sleep(4000);

Pic[5].MoveTo(0,0); Pic[5].ToFront;
Text[4].MoveTo(-540,0);
Text[5] := TextABC.Create(70,570,15,' - Ничё я не гоню, - МС',clwhite);
sleep(4000);

Pic[6].MoveTo(0,0); Pic[6].ToFront;
Text[5].MoveTo(-540,0);
Text[6] := TextABC.Create(20,570,15,' - Ещё скажите, что вы не верите в чудо напиток, - МС',clwhite);
sleep(4000);

Pic[7].MoveTo(0,0); Pic[7].ToFront;
Text[6].MoveTo(-540,0);
Text[7] := TextABC.Create(0,570,13,' - Без этого напитка Сплинтер бы не упрыгал так далеко, - Эйприл',clwhite);
sleep(4000);

Pic[8].MoveTo(0,0); Pic[8].ToFront;
Text[7].MoveTo(-540,0);
Text[8] := TextABC.Create(20,570,15,' - Хм.... Ну возможно история правдивая, - ЧН',clwhite);
sleep(4000);

Pic[9].MoveTo(0,0); Pic[9].ToFront;
Text[8].MoveTo(-540,0);
Text[9] := TextABC.Create(70,570,15,' - Гыгыгыгыгы, - МС',clwhite);
sleep(4000);
CloseGame;
end;

procedure KeyDown(key: integer);
begin
 case key of
 VK_Left: begin If (MS<>nil) and (MS.Visible=True) then begin if jumple=3 then MS.Leftg(jumple-1) else MS.Leftg(jumple) end; end;
 VK_Up:  if (Start.Visible=True) and (Start<>nil) then
   begin
          begin
             case but of 
             1: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); Start.OffLight; Exitb.Light; but:=4; end;
             2: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); BattleMode.OffLight; Start.Light; but:=1; end;
             3: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); Records.OffLight; BattleMode.Light; but:=2; end;
             4: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); Exitb.OffLight; Records.Light; but:=3; end;
             end;
          end;
    end;      
 VK_Down: if (Start.Visible=True) and (Start<>nil) then
   begin
         begin 
            case but of 
            1: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); Start.OffLight; BattleMode.Light; but:=2; end;
            2: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); BattleMode.OffLight; Records.Light; but:=3; end;
            3: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); Records.OffLight; Exitb.Light; but:=4; end;
            4: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); Exitb.OffLight; Start.Light; but:=1; end;
            end;
         end;
    end;     
 VK_Enter: if (Start.Visible=True) and (Start<>nil) then
    begin
         begin
            case but of 
            1: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); Start.OnClick; but:=0; end;
            2: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); BattleMode.OnClick; end;
            3: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); Records.OnClick; end;
            4: begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[1],true); sleep(580); ExitB.OnClick; end;
            end;
         end;
   end;    
 VK_Right: begin If (MS<>nil) and (MS.Visible=True) then begin if jumple=3 then MS.Rightg(jumple-1) else MS.Rightg(jumple) end; end;
 VK_Space: begin If (MS<>nil) and (MS.Visible=True) then begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[4],true); MS.Strike; end; end;
 VK_P: begin Lvlschet; end;
 VK_O: MS.MoveOn(0,-10);
 VK_Q: begin PlusAllVolume(0.05); end;
 VK_A: begin PlusAllVolume(-0.05); end;
 {VK_I: Back[lvlshets].Stop;
 VK_U: GameEnd;
 VK_End: CloseWindow;}
 end;
end;

procedure BattleModeGame;
begin
end;

procedure RecordsSee;
Var 
     i: integer;
     t: array [1..10] of TextABC;
begin
SaveGame;
Start.Visible:=False; Start.b.Visible:=False; 
Exitb.Visible:=False; Exitb.b.Visible:=False;
BattleMode.Visible:=False; BattleMode.b.Visible:=False;
Records.Visible:=False; Records.b.Visible:=False;
For i:=1 to 10 do
      t[i] := TextABC.Create(120,i*56,15,IntToStr(i)+' уровень ' + 'Сыр: ' + IntToStr(Recordsm[i,1]) + ' Высота: ' + IntToStr(recordsm[i,2]) + ' Время: ' + IntToStr(recordsm[i,3]), clGreen);
end;

procedure Down(vn: integer);
var i: integer;
begin
if vn>0 then hi:=hi+vn;
h.Text:='Высота:' + ' ' + IntToStr(hi);
k:=False;
TV.MoveOn(0,vn);    
MS.MoveOn(0,round(vn/2));
Boss[lvlshets].MoveOn(0,vn);   
For i:=1 to d do
    begin
    if NormalDrink[i]<>nil then NormalDrink[i].MoveOn(0,vn);
    if UranDrink[i]<>nil then UranDrink[i].MoveOn(0,vn);
    end;
For i:=1 to n do
    begin
    Table[i].Downing(vn);
    if CheeseH[i]<>nil then CheeseH[i].MoveOn(0,vn);
    end;  
end;

procedure Game(Backs: string; Lvls: string); forward;

procedure Lvlschet;
begin
TimerG.Enabled:=False;
sec:=0;
jumple:=1;
if lvlshets=0 then lvlshets:=1;
inc(lvlshets);
un4seen.Bass.bass.BASS_ChannelPlay(SInG[2],true);
case lvlshets of
2: Game('Back/Zone2.jpg','Levels/Lvl2.txt'); 
3: Game('Back/Zone3.jpg','Levels/Lvl3.txt');
4: Game('Back/Zone4.jpg','Levels/Lvl4.txt');
5: Game('Back/Zone5.jpg','Levels/Lvl5.txt');
6: Game('Back/Zone6.jpg','Levels/Lvl6.txt');
7: Game('Back/Zone7.jpg','Levels/Lvl7.txt');
8: Game('Back/Zone8.jpg','Levels/Lvl8.txt');
9: Game('Back/Zone9.jpg','Levels/Lvl9.txt');
10: Game('Back/Zone10.jpg','Levels/Lvl10.txt');
11: GameEnd;
end;
end;

procedure Jumping;
begin
k:=True;

un4seen.Bass.bass.BASS_ChannelPlay(SInG[6],true);
MS.Jump(Jumple);

if MS.Top<round(WindowHeight/2) then Down(round(WindowHeight/2)-MS.Top);

k:=False;
end;

procedure GameOver;
begin
MS.GameOver;
un4seen.Bass.bass.BASS_ChannelPlay(SInG[5],true);
end;

procedure Gravi;
var i: integer;
    l: boolean;
begin
For i:=1 to n do
  if Table[i]<>nil then
  begin
   if ((MS.Top+36=Table[i].Top) or
       (MS.Top+35=Table[i].Top) or 
       (MS.Top+34=Table[i].Top) or 
       (MS.Top+33=Table[i].Top) or 
       (MS.Top+32=Table[i].Top) or 
       (MS.Top+31=Table[i].Top) or
       (MS.Top+30=Table[i].Top)) and 
       ((MS.Left+27>=Table[i].Left) and (WindowWidth-MS.Left-9>=WindowWidth-Table[i].Left-Table[i].Width)) 
       then 
            begin 
            m:=False; l:=True; break;
            end;
   end;    
if sec>=0 then begin if sec mod 10 = 0 then MS.MSFaceDrink.Width-=3; dec(sec); end;
if sec<0 then jumple:=1;      
For i:=1 to d do
    if NormalDrink[i]<>nil then if NormalDrink[i].Intersect(MS) then begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[3],true); Jumple:=2; NormalDrink[i].MoveTo(-21,0); MS.MSFaceDrink.Width:=43; sec:=130; end;
For i:=1 to d do
    if UranDrink[i]<>nil then if UranDrink[i].Intersect(MS) then begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[3],true); Jumple:=3; UranDrink[i].MoveTo(-21,0); MS.MSFaceDrink.Width:=43; sec:=130; end;    
If (l=True) and (k=False) then begin Jumping end else begin MS.MoveOn(0,5) end;
l:=False;

dt.ms+=50;
if dt.ms>=1000 then begin dt.s+=1; dt.ms:=0; end;
if dt.s>=60 then begin dt.m+=1; dt.s:=0; end;
time.Text := 'Время: '+ dt.m.ToString + ':' + dt.s.ToString + ':' + (dt.ms/10).ToString;
end;

procedure HelpGraviThread;
var i: integer;
begin
While TimerG.Enabled=True do
begin
if MS.Intersect(TV) then Lvlschet;  
For i:=1 to ch do
    if CheeseH[i]<>nil then if CheeseH[i].InterSect(MS) then begin un4seen.Bass.bass.BASS_ChannelPlay(SInG[3],true); CheeseH[i].MoveTo(-21,0); schh:=schh+1; Cheese.Text:='Сыр: ' + IntToStr(schh); end;   
if Boss[lvlshets]<>nil then begin
                                    if (Boss[lvlshets].Intersect(MS)) and (m=False) then begin m:=True; if MS.MSFaceLive.Width>1 then MS.MSFaceLive.Width-=3 else GameOver; end;
                                    end;
if MS.Suriken.Intersect(Boss[lvlshets]) then begin Boss[Lvlshets].MoveGame; MS.Suriken.MoveTo(-36,-36); end;
if MS.Top>WindowHeight then begin TimerG.Enabled:=False; GameOver; end;                                     
end;
end;

procedure Game(Backs: string; Lvls: string);
var r,i,m,l,sch,j,ss: integer;
    ur: single;
begin
if lvlshets>1 then 
   begin
   if schh>recordsm[lvlshets-1,1] then recordsm[lvlshets-1,1]:=schh;
   if hi>recordsm[lvlshets-1,2] then recordsm[lvlshets-1,2]:=hi;
   if dt.s*1000+dt.ms+dt.m*60000<recordsm[lvlshets-1,3] then recordsm[lvlshets-1,3]:=dt.s*1000+dt.ms+dt.m*60000;
   SaveGame;
   end;
un4seen.Bass.Bass.BASS_ChannelGetAttribute(Back[lvlshets-1].h, un4seen.Bass.BassAttribute.BASS_ATTRIB_VOL, ur);
Back[0].Stop;
Start.Visible:=False; Start.b.Visible:=False; 
Exitb.Visible:=False; Exitb.b.Visible:=False;
BattleMode.Visible:=False; BattleMode.b.Visible:=False;
Records.Visible:=False; Records.b.Visible:=False;

if lvlshets>1 then begin Back[lvlshets-1].Visible:=False; Back[lvlshets-1].Stop; end;
Back[lvlshets]:= BackGrounds.Create(0,0,Backs,'Sounds/Music/Track'+IntToStr(lvlshets)+'.mp3');
un4seen.Bass.Bass.BASS_ChannelSetAttribute(Back[lvlshets].h, un4seen.Bass.BASSAttribute.BASS_ATTRIB_VOL,ur);
assign(input,Lvls);
reset(input);

For i:=1 to n do
   begin
   if Table[n]=nil then else Table[n].Destroy;
   end;

m:=0;
l:=0;
sch:=0;

For i:=1 to 192 do
  begin
  read(r);
  if m>=6 then begin m:=0; l:=l+1; end;
  m:=m+1;
  if r>0 then
    begin
    sch:=sch+1;
    Table[sch] := Platform.Create(m*90-90,l*20-20,r);
    Table[sch].Transparent:=True;
    end;  
  end;

m:=0;
l:=0;

While Eof(input)<>True do
   begin
   read(r);
   if m>=6 then begin m:=0; l:=l+1; end;
   m:=m+1;
   if r>0 then
      begin
      sch:=sch+1;
      Table[sch] := Platform.Create(m*90-90,-l*20,r);
      Table[sch].Transparent:=True;
      end;  
   end;

close(input);

if TV=nil then 
  begin
  TV := PictureABC.Create(Table[sch].Left+45-21,-l*20-40,'Things/TV.png');
  TV.Transparent:=True;
  end
  else TV.MoveTo(Table[sch].Left+45-21,-l*20-40);
TV.ToFront;  
ss:=sch;
sch:=0;

For i:=1 to n-1 do
   if (Table[i]<>nil) and (i<>ss) then
      begin
        r:=random(2);
        if r=1 then
           begin
           sch:=sch+1;
           if CheeseH[sch]=nil then
           begin
           CheeseH[sch] := PictureABC.Create(Table[i].Left+35,Table[i].Top-17,'Things/Cheese.png');
           CheeseH[sch].Transparent:=True;
           end else begin CheeseH[sch].MoveTo(Table[i].Left+35,Table[i].Top-17); CheeseH[sch].ToFront; end;
           end;
      end;

sch:=0;

For i:=1 to n-1 do
   if Table[i]<>nil then
      begin
        r:=random(20);
        if (r=1) or (r=11) then
           begin
           sch:=sch+1;
             if r=11 then
              begin
              if UranDrink[sch]=nil then
               begin
               UranDrink[sch] := PictureABC.Create(Table[i].Left+45-4,Table[i].Top-16,'Things/UranAlepahtoDrink.png');
               UranDrink[sch].Transparent:=True;
               end else 
               begin 
               UranDrink[sch].MoveTo(Table[i].Left+45-4,Table[i].Top-16); UranDrink[sch].ToFront; 
               end;
              end
              else
              begin
               if NormalDrink[sch]=nil then
               begin
               NormalDrink[sch] := PictureABC.Create(Table[i].Left+45-4,Table[i].Top-16,'Things/NormalAlepahtoDrink.png');
               NormalDrink[sch].Transparent:=True;
               end else 
               begin 
               NormalDrink[sch].MoveTo(Table[i].Left+45-4,Table[i].Top-16); NormalDrink[sch].ToFront; 
               end;
              end;
          end;
      end;

For i:=1 to ch do
    for j:=1 to d do
        begin
        if cheeseh[i]<>nil then 
          begin
          if NormalDrink[j]<>nil then
             if cheeseh[i].Intersect(NormalDrink[j]) then NormalDrink[j].MoveTo(-21,0);
          if UranDrink[j]<>nil then 
             if cheeseh[i].Intersect(UranDrink[j]) then UranDrink[j].MoveTo(-21,0);
          end;   
        end;

if MenuP=nil then 
   begin
   MenuP:=PictureABC.Create(0,0,'Pictures/Block.png');
   MenuP.Width:=WindowWidth;
   end;
MenuP.ToFront;   
if Cheese=nil then    
   begin
   Cheese:=TextABC.Create(110,5,17,'Сыр: 0',clYellow);
   h:=TextABC.Create(200,5,17,'Высота: 0',clBlue);
   time:=TextABC.Create(350,5,17,'Время: 0:0:0', clOrange);
   end;   

if lvlshets>1 then begin Boss[lvlshets-1].TimerV.Enabled:=False; Boss[lvlshets-1].MoveTo(-200,-200); end;
Boss[lvlshets] := UnFriend.Create(WindowWidth+72,round(WindowHeight/2)-72,lvlshets);

schh:=0;
hi:=0;   
Cheese.Text:='Сыр: 0'; Cheese.ToFront;
h.Text:='Высота: 0'; h.ToFront;
time.Text:='Время: 0:0:0'; dt.m:=0; dt.s:=0; dt.ms:=0; time.ToFront;
Redraw;

if MS=nil then
   begin
     MS := Splinter.Create(240,580,'Splinter/MS.spinf');
  end else  
MS.MoveTo(240,580);
MS.ToFront;  
MS.MSFace.ToFront;
MS.MSFaceFrame.ToFront;
MS.MSFaceDrink.ToFront;
MS.MSFaceLive.ToFront;
MS.Jump(1);
jumple:=1;

if TimerG=nil then 
   TimerG := Timer.Create(50,Gravi);
TimerG.Enabled:=True;
if TimerHelp.ThreadState<>ThreadState.Running then TimerHelp.Start;
end;

procedure GameStart;
begin
lvlshets:=1;
Game('Back/Zone1.jpg','Levels/Lvl1.txt');
end;

procedure Size;
begin
SetWindowWidth(540);
SetWindowHeight(640);
end;

procedure Menu;
var i, j: integer;
begin
GraphABC.MainForm.Cursor.Dispose;
OnkeyDown:=KeyDown;
Logo := Icon.Create('IconGame.ico');
GraphABC.MainForm.Icon:=Logo;
Size;
SetWindowIsFixedSize(true);
CenterWindow;
SetWindowCaption('Splinter - Jump');
GraphABC.MainForm.MaximizeBox:=False;

for i:=1 to 10 do
    recordsm[i,3]:=2147483647;

For i:=1 to 10 do
     SInG[i]:=-1;
     
BASS_Init(-1,4410,un4seen.Bass.BASSInit.BASS_DEVICE_SPEAKERS,sysinptr);     
SInG[1] :=BASS_StreamCreateFile('Sounds/Tune/Button.wav',0,0,un4seen.Bass.BASSFlag.BASS_DEFAULT); 

BASS_Init(-1,4410,un4seen.Bass.BASSInit.BASS_DEVICE_SPEAKERS,sysinptr);     
SInG[2] :=BASS_StreamCreateFile('Sounds/Tune/TV.wav',0,0,un4seen.Bass.BASSFlag.BASS_DEFAULT); 

BASS_Init(-1,4410,un4seen.Bass.BASSInit.BASS_DEVICE_SPEAKERS,sysinptr);     
SInG[3] :=BASS_StreamCreateFile('Sounds/Tune/Thing.wav',0,0,un4seen.Bass.BASSFlag.BASS_DEFAULT); 

BASS_Init(-1,4410,un4seen.Bass.BASSInit.BASS_DEVICE_SPEAKERS,sysinptr);     
SInG[4] :=BASS_StreamCreateFile('Sounds/Tune/SurikenStrike.wav',0,0,un4seen.Bass.BASSFlag.BASS_DEFAULT); 

BASS_Init(-1,4410,un4seen.Bass.BASSInit.BASS_DEVICE_SPEAKERS,sysinptr);     
SInG[5] :=BASS_StreamCreateFile('Sounds/Tune/Pad.wav',0,0,un4seen.Bass.BASSFlag.BASS_DEFAULT); 

BASS_Init(-1,4410,un4seen.Bass.BASSInit.BASS_DEVICE_SPEAKERS,sysinptr);     
SInG[6] :=BASS_StreamCreateFile('Sounds/Tune/Jump.wav',0,0,un4seen.Bass.BASSFlag.BASS_DEFAULT); 

BASS_Init(-1,4410,un4seen.Bass.BASSInit.BASS_DEVICE_SPEAKERS,sysinptr);     
SInG[7] :=BASS_StreamCreateFile('Sounds/Music/Track11.mp3',0,0,un4seen.Bass.BASSFlag.Bass_Sample_Loop); 

Back[0] := BackGrounds.Create(0,0,'Pictures/Menu.jpg','Sounds/Music/Track0.mp3');

Start := Button.Create(round(WindowWidth/3.1),round(WindowHeight/1.7),200,40,'Начать игру',clgreen,cllightgreen);
BattleMode := Button.Create(round(WindowWidth/3.1),round(WindowHeight/1.52),200,40,'Поставить рекорд',clgreen,cllightgreen);
Records := Button.Create(round(WindowWidth/3.1),round(WindowHeight/1.375),200,40,'Рекорды',clgreen,cllightgreen);
Exitb := Button.Create(round(WindowWidth/3.1),round(WindowHeight/1.255),200,40,'Выход',clgreen,cllightgreen);

Start.Light;
Start.Visible:=True;
but:=1;

assign(input,'Records.dat');
reset(input);
for i:=1 to 10 do
   begin
   for j:=1 to 2 do
   read(input,recordsm[i,j]);
   end;
close(input);   

TimerHelp := System.Threading.Thread.Create(HelpGraviThread);

Start.OnClick:=GameStart; 
Exitb.OnClick:=CloseGame;
BattleMode.OnClick:=BattleModeGame;
Records.OnClick:=Recordssee;
end;

begin
Menu;
End.

  